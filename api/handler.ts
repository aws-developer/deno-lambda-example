import {
    APIGatewayProxyEvent2,
    Context,
} from "https://deno.land/x/lambda/mod.ts";

import { validate } from 'https://deno.land/x/deno_class_validator/mod.ts';
import { BodyModel } from "./validatorBody.ts";

function ok(body: unknown, statusCode = 200) {
    return {
        statusCode,
        body
    };
}
function error(message: string, data: unknown, statusCode = 500) {
    return ok({ message: message, errors: data }, statusCode);
}


export async function submit(event: APIGatewayProxyEvent2, _context: Context) {
    try{
        if(event !== null && event !== undefined){
            if(event.body === null || event.body === undefined){
                return error("invalid input","Body is required", 422);
            }
        } else {
            return error("invalid input","Body is required", 422);
        }
        
        let requestBody = event.body;
        console.log(requestBody);
        const { name } = requestBody;
    
        let body = new BodyModel();
        body.name = name;
        
        let err = new Array();
        
        err = await validate(body, { validationError: { target: false } });
        
        if (err !== null && err?.length > 0 ) {
           return ok({ message: `Validation value failed`, errors: err }, 422);
        }
        
        return ok({
            message: `Sucessfully submitted`,
            data: name,
        });
        
    } catch (Error) {
        return error(`Internal server error`, null, 500);
    }
}