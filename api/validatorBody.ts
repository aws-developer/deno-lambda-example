import { IsNotEmpty, MaxLength, MinLength,IsAlphanumeric } from 'https://deno.land/x/deno_class_validator/mod.ts';

export interface Body {
    first_name: string
}

export class BodyModel {
    @IsNotEmpty({
          message: "The name is required."
    })
    @MaxLength(100, {
          message: "The name must be shorter than or equal to $constraint1 characters."
    })
    @MinLength(3,{
        message: "The name must be shorter than or equal to $constraint1 characters."
    })
    @IsAlphanumeric()
    name?: string;
}